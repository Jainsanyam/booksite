import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { EventEmitter } from 'protractor';

@Component({
  selector: 'app-competition',
  templateUrl: './competition.component.html',
  styleUrls: ['./competition.component.css'],
})
export class CompetitionComponent implements OnInit {
  @Input() name: any;
  @Output() arr = new EventEmitter<Competitor>();
  constructor() {}

  ngOnInit(): void {}
  add() {
    this.arr.push(this.name);
  }
}
