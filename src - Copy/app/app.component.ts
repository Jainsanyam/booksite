import { Component } from '@angular/core';
import { Movie } from './movie';
import { moviedb } from './movie';
import { CurrencyPipe } from '@angular/common';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { NONE_TYPE } from '@angular/compiler';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  temparray: any[];
  temparray2: any[];
  products: any[] = [
    { product: 'Pepsi', sales: [2, 5, 8, 10, 5] },
    { product: 'Coke', sales: [3, 6, 5, 4, 11, 5] },
    { product: '5Star', sales: [10, 14, 22] },
    { product: 'Maggi', sales: [3, 3, 3, 3, 3] },
    { product: 'Perk', sales: [1, 2, 1, 2, 1, 2] },
    { product: 'Bingo', sales: [0, 1, 0, 3, 2, 6] },
    { product: 'Gems', sales: [3, 3, 1, 1] },
  ];
  arr1: any[] = [];
  check: boolean = false;

  ngOnInit() {
    this.totalsale();
  }
  totalsale() {
    this.temparray = [...this.products];
    var len = this.temparray.length;
    for (var i = 0; i < len; i++) {
      var sum = 0;
      var total = this.temparray[i].sales;
      for (var j = 0; j < total.length; j++) {
        sum += total[j];
      }
      this.products[i].totalsales = sum;
    }
  }
  detail(i) {
    this.arr1.push(this.temparray[i]);
    console.log(this.arr1);
  }
  sort() {
    this.check = true;
    this.temparray2 = [...this.temparray];
    this.temparray2.sort(this.sortedarray);
    console.log(this.temparray2);
  }
  sortedarray(a, b) {
    if (a.product > b.product) {
      return 1;
    } else if (a.product == b.product) {
      return 0;
    } else {
      return -1;
    }
  }
}
