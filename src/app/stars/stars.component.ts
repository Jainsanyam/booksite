import { Component, OnInit } from "@angular/core";
import { NetService } from "../net.service";
@Component({
    selector: "app-stars",
    templateUrl: "./stars.component.html",
    styleUrls: ["./stars.component.css"],
})
export class StarsComponent implements OnInit {
    url = "http://localhost:2410/sporticons/stars";
    strData = [];
    constructor(private netService: NetService) {}

    ngOnInit() {
        this.netService.getData(this.url).subscribe((resp) => {
            // console.log(resp)

            this.strData.push(resp);
            let data = resp;
        });
        console.log(this.strData);
    }
}
