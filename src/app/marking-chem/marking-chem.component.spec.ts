import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkingChemComponent } from './marking-chem.component';

describe('MarkingChemComponent', () => {
  let component: MarkingChemComponent;
  let fixture: ComponentFixture<MarkingChemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarkingChemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkingChemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
