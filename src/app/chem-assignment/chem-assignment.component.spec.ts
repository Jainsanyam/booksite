import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChemAssignmentComponent } from './chem-assignment.component';

describe('ChemAssignmentComponent', () => {
  let component: ChemAssignmentComponent;
  let fixture: ComponentFixture<ChemAssignmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChemAssignmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChemAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
