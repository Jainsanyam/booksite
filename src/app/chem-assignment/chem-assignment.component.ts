import { Component, OnInit, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-chem-assignment",
  templateUrl: "./chem-assignment.component.html",
  styleUrls: ["./chem-assignment.component.css"],
})
export class ChemAssignmentComponent implements OnInit {
  @Output() compChanged = new EventEmitter();
  answerSheet: any[] = [];
  ans: string = " ";
  answer: string = "Your Answer: Not Answered";

  temp: number = 0;
  questions: any[] = [
    {
      text: "What is the melting point of ice",
      options: ["0F", "0C", "100C", "100F"],
      answer: 2,
    },
    {
      text: "What is the atomic number of Oxygen",
      options: ["6", "7", "8", "9"],
      answer: 3,
    },
    {
      text: "What is the atomic number of Carbon",
      options: ["6", "7", "8", "9"],
      answer: 1,
    },
    {
      text: "Which of these is an inert element",
      options: ["Flourine", "Suphur", "Nitrogen", "Argon"],
      answer: 4,
    },
    {
      text: "What is 0 Celsius in Fahrenheit",
      options: ["0", "32", "20", "48"],
      answer: 2,
    },
  ];

  previousQust() {
    this.temp -= 1;
  }

  choosedAnswer(index) {
    console.log(index);
    if (index == 0) {
      this.answer = " Your Answer: A";
      this.ans = "A";
    } else if (index == 1) {
      this.answer = " Your Answer: B";
      this.ans = "B";
    } else if (index == 2) {
      this.answer = " Your Answer: C";
      this.ans = "C";
    } else if (index == 3) {
      this.answer = " Your Answer: D";
      this.ans = "D";
    }
  }
  nexQuestion() {
    if (this.temp == this.questions.length - 1) {
      this.temp = this.questions.length - 1;
    } else {
      this.temp += 1;
    }
    this.answerSheet.push(this.ans);
    // console.log(this.answerSheet);
    this.answer = "Your Answer: Not Answered";
    this.ans = "";

    this.compChanged.emit(this.answerSheet);
  }

  constructor() {}

  ngOnInit() {}
}
