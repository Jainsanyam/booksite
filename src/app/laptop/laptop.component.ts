import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-laptop',
  templateUrl: './laptop.component.html',
  styleUrls: ['./laptop.component.css'],
})
export class LaptopComponent implements OnInit {
  laptop: any = [
    {
      name: 'Acer ',
      brand: 'M205',

      price: 25000,
    },
    {
      name: 'Dell',
      brand: 'Vostro6',

      price: 31000,
    },
    {
      name: 'Lenovo',
      brand: 'Yoga 40',

      price: 27500,
    },
    {
      name: 'HP',
      brand: 'H620',

      price: 29000,
    },
  ];
  constructor() {}

  ngOnInit(): void {}
}
