import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  mobiles: any = [
    {
      name: 'Virat Kohli',
      country: 'India',

      sport: 'Cricket',
    },
    {
      name: 'MS Dhoni',
      country: 'India',

      sport: 'Cricket',
    },
    {
      name: 'Rohit Sharma',
      country: 'India',

      sport: 'Cricket',
    },
    {
      name: 'David Warner',
      country: 'Australia',

      sport: 'Cricket',
    },
    {
      name: 'Cristiano Ronaldo',
      country: 'Portugal',

      sport: 'Football',
    },
    {
      name: 'Lionel Messi',
      country: 'Argentina',

      sport: 'Football',
    },
    {
      name: 'Neymar Jr.',
      country: 'Brazil',

      sport: 'Football',
    },
    {
      name: 'Kylian Mbappe',
      country: 'France',

      sport: 'Football',
    },
  ];
  constructor(private authService: AuthService) {}

  ngOnInit(): void {}
}
