import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkingMathsComponent } from './marking-maths.component';

describe('MarkingMathsComponent', () => {
  let component: MarkingMathsComponent;
  let fixture: ComponentFixture<MarkingMathsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarkingMathsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkingMathsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
