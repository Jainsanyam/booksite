import { Component, OnInit } from "@angular/core";
import { NetService } from "../net.service";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
@Component({
    selector: "app-details",
    templateUrl: "./details.component.html",
    styleUrls: ["./details.component.css"],
})
export class DetailsComponent implements OnInit {
    id: string;
    strData = [];
    dataa: any[];
    constructor(
        private netService: NetService,
        private route: ActivatedRoute,
        private router: Router
    ) {}
    url = "http://localhost:2410/sporticons/stars";
    ngOnInit() {
        this.route.paramMap.subscribe((params) => {
            this.id = params.get("id");
            console.log(this.id);
        });
        this.netService.getData(this.url).subscribe((resp) => {
            this.strData.push(resp);
            //  console.log(this.strData)
            let data = resp;
            console.log(data);
            this.dataa = data.filter((player) => player.id == this.id);
            console.log(this.dataa);
        });
        this.makeData();
    }
    makeData() {
        // this.data = this.strData.filter(player =>player.id == this.id)
        // console.log(this.data)
    }
}
