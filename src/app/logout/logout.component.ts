import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css'],
})
export class LogoutComponent implements OnInit {
  mobiles: any = [
    {
      name: 'Virat Kohli',
      country: 'India',

      sport: 'Cricket',
    },
    {
      name: 'MS Dhoni',
      country: 'India',

      sport: 'Cricket',
    },
    {
      name: 'Rohit Sharma',
      country: 'India',

      sport: 'Cricket',
    },
    {
      name: 'David Warner',
      country: 'Australia',

      sport: 'Cricket',
    },
  ];
  constructor() {}

  ngOnInit(): void {}
}
