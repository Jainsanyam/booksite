import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-choosenames',
  templateUrl: './choosenames.component.html',
  styleUrls: ['./choosenames.component.css'],
})
export class ChoosenamesComponent implements OnInit {
  @Input() namesRadio;
  @Output() optSel = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}
  emitChange() {
    this.optSel.emit();
  }
}
