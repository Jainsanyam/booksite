import { BooksNetService } from "./books-net.service";
import { HttpClientModule } from "@angular/common/http";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule } from "@angular/forms";
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { BookComponent } from './book/book.component';
import { LeftPanelComponent } from './left-panel/left-panel.component';

@NgModule({
  declarations: [AppComponent, NavBarComponent, BookComponent, LeftPanelComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [BooksNetService],
  bootstrap: [AppComponent],
})
export class AppModule {}
