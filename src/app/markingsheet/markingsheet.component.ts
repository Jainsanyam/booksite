import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-markingsheet",
  templateUrl: "./markingsheet.component.html",
  styleUrls: ["./markingsheet.component.css"],
})
export class MarkingsheetComponent implements OnInit {
  @Input() ansSheet: any;

  constructor() {}

  ngOnInit() {
    console.log(this.ansSheet);
  }
}
