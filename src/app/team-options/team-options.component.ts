import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-team-options',
  templateUrl: './team-options.component.html',
  styleUrls: ['./team-options.component.css'],
})
export class TeamOptionsComponent implements OnInit {
  @Input() playersCB;
  @Input() sportsRadio;
  @Output() optSel = new EventEmitter();
  mobile: any = ['32GB', '64GB', '128GB'];
  constructor() {}

  ngOnInit() {
    console.log('In TeamOptionsComponent');
    console.log('PlayersCB', this.playersCB);
    console.log('sportsRadio', this.sportsRadio);
  }
  emitChange() {
    this.optSel.emit();
  }
}
