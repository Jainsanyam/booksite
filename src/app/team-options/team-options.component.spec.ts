import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamOptionsComponent } from './team-options.component';

describe('TeamOptionsComponent', () => {
  let component: TeamOptionsComponent;
  let fixture: ComponentFixture<TeamOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
