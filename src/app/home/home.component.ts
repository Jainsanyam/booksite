import { Component, OnInit } from '@angular/core';
import { AuthService } from './../auth.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  constructor(private authService: AuthService) {}

  ngOnInit() {}

  mobiles: any = [
    {
      name: 'Realme 3',
      brand: 'Realme',
      qty: 1,
      price: 9999,
    },
    {
      name: 'On7',
      brand: 'Samsung',
      qty: 1,
      price: 11999,
    },
  ];
}
