import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkingCompComponent } from './marking-comp.component';

describe('MarkingCompComponent', () => {
  let component: MarkingCompComponent;
  let fixture: ComponentFixture<MarkingCompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarkingCompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkingCompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
