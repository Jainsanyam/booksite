import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MathsAssignmentComponent } from './maths-assignment.component';

describe('MathsAssignmentComponent', () => {
  let component: MathsAssignmentComponent;
  let fixture: ComponentFixture<MathsAssignmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MathsAssignmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MathsAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
