import { Component, OnInit, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-maths-assignment",
  templateUrl: "./maths-assignment.component.html",
  styleUrls: ["./maths-assignment.component.css"],
})
export class MathsAssignmentComponent implements OnInit {
  @Output() compChanged = new EventEmitter();
  answerSheet: any[] = [];
  ans: string = " ";
  answer: string = "Your Answer: Not Answered";

  temp: number = 0;
  questions: any[] = [
    {
      text: "What is 8 * 9",
      options: ["72", "76", "64", "81"],
      answer: 1,
    },
    {
      text: "What is 2*3+4*5",
      options: ["70", "50", "26", "60"],
      answer: 3,
    },
  ];

  previousQust() {
    this.temp -= 1;
  }

  choosedAnswer(index) {
    console.log(index);
    if (index == 0) {
      this.answer = " Your Answer: A";
      this.ans = "A";
    } else if (index == 1) {
      this.answer = " Your Answer: B";
      this.ans = "B";
    } else if (index == 2) {
      this.answer = " Your Answer: C";
      this.ans = "C";
    } else if (index == 3) {
      this.answer = " Your Answer: D";
      this.ans = "D";
    }
  }
  nexQuestion() {
    if (this.temp == this.questions.length - 1) {
      this.temp = this.questions.length - 1;
    } else {
      this.temp += 1;
    }
    this.answerSheet.push(this.ans);
    // console.log(this.answerSheet);
    this.answer = "Your Answer: Not Answered";
    this.ans = "";

    this.compChanged.emit(this.answerSheet);
  }

  constructor() {}

  ngOnInit() {}
}
