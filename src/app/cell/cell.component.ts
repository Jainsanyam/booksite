import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-cell",
  templateUrl: "./cell.component.html",
  styleUrls: ["./cell.component.css"],
})
export class CellComponent implements OnInit {
  @Input() cell: any;
  @Input() index: number;

  @Output() compChanged = new EventEmitter();
  constructor() {}

  ngOnInit() {}
  check(i) {
    this.compChanged.emit({ value: this.cell, index: this.index });
  }
}
