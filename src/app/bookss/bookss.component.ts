import { BooksNetService } from "./../books-net.service";
import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
@Component({
  selector: "app-bookss",
  templateUrl: "./bookss.component.html",
  styleUrls: ["./bookss.component.css"],
})
export class BookssComponent implements OnInit {
  @Input() number;
  @Output() compChanged = new EventEmitter();
  type: string = "";
  url1 = "";
  constructor(
    private bookService: BooksNetService,
    private route: ActivatedRoute,
    private router: Router
  ) {}
  bookData: any[];
  startPage: number = 0;
  endPage: number = 0;
  totalPage: any;
  // maxPage: any;
  currPage = 1;
  maxPage = 15;
  filteredBook: any[];
  ngOnInit() {
    this.url1 = "http://localhost:2410/booksapp/books?page=" + this.currPage;
    this.bookService.getData(this.url1).subscribe((resp) => {
      var a = resp.data;
      console.log(a);
      var b = resp.pageInfo;
      let sPage = a[0].bookid;
      let lPage = a[9].bookid;

      this.endPage = lPage;
      this.startPage = sPage;

      this.totalPage = b.totalItemCount;
    });

    this.route.paramMap.subscribe((params) => {
      this.type = params.get("genre");
      console.log(this.type);
    });
    if (this.type == null) {
      this.url1 = "http://localhost:2410/booksapp/books?page=" + this.currPage;
      this.bookService.getData(this.url1).subscribe((resp) => {
        //  this.strData.push(resp)
        //  let data = resp;
        this.bookData = resp.data;
        console.log(resp.data);
        console.log(resp);
      });
    }
  }
  nextPage(x: number) {
    this.currPage = this.number;
    this.currPage = this.currPage + x;
    let obj = this.currPage;
    let path = "/books/";

    this.router.navigate([path], {
      queryParams: { page: obj },
    });

    this.compChanged.emit(this.currPage);
    this.optChange(0);
  }
  optChange(a) {
    console.log(a);
    this.type = a;
    console.log(this.type);
    if (this.type == "") {
      this.url1 = "http://localhost:2410/booksapp/books?page=" + this.currPage;
      this.bookService.getData(this.url1).subscribe((resp) => {
        //  this.strData.push(resp)
        //  let data = resp;
        this.bookData = resp.data;
        console.log(resp.data);
        console.log(resp);
      });
    }

    if (this.type == "Children") {
      this.url1 =
        "http://localhost:2410/booksapp/books/" +
        this.type +
        "/?page=" +
        this.currPage;
      // "?page=" +
      // this.currPage;
      this.bookService.getData(this.url1).subscribe((resp) => {
        //  this.strData.push(resp)
        //  let data = resp;
        this.bookData = resp.data;
        console.log(resp.data);
      });
    }
    if (this.type == "Fiction") {
      this.url1 =
        "http://localhost:2410/booksapp/books/" +
        this.type +
        "/?page=" +
        this.currPage;
      // "?page=" +
      // this.currPage;
      this.bookService.getData(this.url1).subscribe((resp) => {
        //  this.strData.push(resp)
        //  let data = resp;
        this.bookData = resp.data;
        console.log(resp.data);
      });
    }
    if (this.type == "Mystery") {
      this.url1 =
        "http://localhost:2410/booksapp/books/" +
        this.type +
        "/?page=" +
        this.currPage;
      // "?page=" +
      // this.currPage;
      this.bookService.getData(this.url1).subscribe((resp) => {
        //  this.strData.push(resp)
        //  let data = resp;
        this.bookData = resp.data;
        console.log(resp.data);
      });
    }
    if (this.type == "Management") {
      this.url1 =
        "http://localhost:2410/booksapp/books/" +
        this.type +
        "/?page=" +
        this.currPage;

      // "?page=" +
      // this.currPage;
      this.bookService.getData(this.url1).subscribe((resp) => {
        //  this.strData.push(resp)
        //  let data = resp;
        this.bookData = resp.data;
        console.log(resp.data);
      });
    }
    if (this.type == "	Self Help") {
      this.url1 =
        "http://localhost:2410/booksapp/books/" +
        this.type +
        "/?page=" +
        this.currPage;

      this.bookService.getData(this.url1).subscribe((resp) => {
        //  this.strData.push(resp)
        //  let data = resp;
        this.bookData = resp.data;
        console.log(resp.data);
      });
    }
    this.route.queryParamMap.subscribe((params) => {
      var p1Value = params.get("newarrival");
      this.type = p1Value;
      if (this.type == "Yes") {
        this.url1 =
          "http://localhost:2410/booksapp/books?newarrival=Yes+/?page=" +
          this.currPage;
        this.url1 = "http://localhost:2410/booksapp/books?newarrival=Yes";
        // "?page=" +
        // this.currPage;
        this.bookService.getData(this.url1).subscribe((resp) => {
          //  this.strData.push(resp)
          //  let data = resp;
          this.bookData = resp.data;
          console.log(resp.data);
        });
      }
    });
  }
}
