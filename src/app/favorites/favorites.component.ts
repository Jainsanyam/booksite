import { Photo } from './../photo';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { from } from 'rxjs';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.css'],
})
export class FavoritesComponent implements OnInit {
  @Input() photo: Photo;
  @Output() compChanged = new EventEmitter<Photo>();
  constructor() {}

  ngOnInit() {}
  removeFromArray() {
    this.compChanged.emit(this.photo);
  }
}
