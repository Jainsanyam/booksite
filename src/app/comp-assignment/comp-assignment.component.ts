import { Component, OnInit, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-comp-assignment",
  templateUrl: "./comp-assignment.component.html",
  styleUrls: ["./comp-assignment.component.css"],
})
export class CompAssignmentComponent implements OnInit {
  @Output() compChanged = new EventEmitter();
  answerSheet: any[] = [];
  ans: string = " ";
  answer: string = "Your Answer: Not Answered";

  temp: number = 0;
  questions: any[] = [
    {
      text: "How many bytes are there in 1 kilobyte",
      options: ["16", "256", "1024", "4096"],
      answer: 3,
    },
    {
      text: "Who developed ReactJS",
      options: ["Facebook", "Google", "Microsoft", "Apple"],
      answer: 1,
    },
    {
      text: "Angular is supported by ",
      options: ["Facebook", "Google", "Microsoft", "Twitter"],
      answer: 2,
    },
    {
      text: "C# was developed by ",
      options: ["Amazon", "Google", "Microsoft", "Twitter"],
      answer: 3,
    },
    {
      text: "Bootstrap was developed by ",
      options: ["Apple", "Google", "Facebook", "Twitter"],
      answer: 4,
    },
    {
      text: "AWS is provided by ",
      options: ["Apple", "Amazon", "Microsoft", "Google"],
      answer: 2,
    },
    {
      text: "Azure is provided by ",
      options: ["Microsoft", "Amazon", "IBM", "Google"],
      answer: 1,
    },
    {
      text: "Angular is a framework that uses ",
      options: ["Java", "Python", "C#", "Typescript"],
      answer: 4,
    },
  ];

  previousQust() {
    this.temp -= 1;
  }

  choosedAnswer(index) {
    console.log(index);
    if (index == 0) {
      this.answer = " Your Answer: A";
      this.ans = "A";
    } else if (index == 1) {
      this.answer = " Your Answer: B";
      this.ans = "B";
    } else if (index == 2) {
      this.answer = " Your Answer: C";
      this.ans = "C";
    } else if (index == 3) {
      this.answer = " Your Answer: D";
      this.ans = "D";
    }
  }
  nexQuestion() {
    if (this.temp == this.questions.length - 1) {
      this.temp = this.questions.length - 1;
    } else {
      this.temp += 1;
    }
    console.log(this.temp);
    this.answerSheet.push(this.ans);
    // console.log(this.answerSheet);
    this.answer = "Your Answer: Not Answered";
    this.ans = "";

    this.compChanged.emit(this.answerSheet);
  }

  constructor() {}

  ngOnInit() {}
}
