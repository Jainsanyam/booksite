import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompAssignmentComponent } from './comp-assignment.component';

describe('CompAssignmentComponent', () => {
  let component: CompAssignmentComponent;
  let fixture: ComponentFixture<CompAssignmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompAssignmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompAssignmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
