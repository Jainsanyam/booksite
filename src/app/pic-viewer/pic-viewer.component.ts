import { Photo } from './../photo';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pic-viewer',
  templateUrl: './pic-viewer.component.html',
  styleUrls: ['./pic-viewer.component.css'],
})
export class PicViewerComponent implements OnInit {
  @Input() photo: Photo;
  @Output() compChanged = new EventEmitter<Photo>();
  constructor() {}

  ngOnInit() {}
  addToFavroite() {
    this.compChanged.emit(this.photo);
  }
}
