import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-question",
  templateUrl: "./question.component.html",
  styleUrls: ["./question.component.css"],
})
export class QuestionComponent implements OnInit {
  @Input() quest: any;
  @Output() compChanged = new EventEmitter();
  answerSheet: any[] = [];
  ans: string = " ";
  answer: string = "Your Answer: Not Answered";
  constructor() {}
  temp: number = 0;
  questions: any[] = [
    {
      text: "What is the capital of India",
      options: ["New Delhi", "London", "Paris", "Tokyo"],
      answer: 1,
    },
    {
      text: "What is the capital of Italy",
      options: ["Berlin", "London", "Rome", "Paris"],
      answer: 3,
    },
    {
      text: "What is the capital of China",
      options: ["Shanghai", "HongKong", "Shenzen", "Beijing"],
      answer: 4,
    },
    {
      text: "What is the capital of Nepal",
      options: ["Tibet", "Kathmandu", "Colombo", "Kabul"],
      answer: 2,
    },
    {
      text: "What is the capital of Iraq",
      options: ["Baghdad", "Dubai", "Riyadh", "Teheran"],
      answer: 1,
    },
    {
      text: "What is the capital of Bangladesh",
      options: ["Teheran", "Kabul", "Colombo", "Dhaka"],
      answer: 4,
    },
    {
      text: "What is the capital of Sri Lanka",
      options: ["Islamabad", "Colombo", "Maldives", "Dhaka"],
      answer: 2,
    },
    {
      text: "What is the capital of Saudi Arabia",
      options: ["Baghdad", "Dubai", "Riyadh", "Teheran"],
      answer: 1,
    },
    {
      text: "What is the capital of France",
      options: ["London", "New York", "Paris", "Rome"],
      answer: 3,
    },
    {
      text: "What is the capital of Italy",
      options: ["Berlin", "London", "Paris", "Rome"],
      answer: 4,
    },
    {
      text: "What is the capital of Sweden",
      options: ["Helsinki", "Stockholm", "Copenhagen", "Lisbon"],
      answer: 2,
    },
    {
      text: "What is the currency of UK",
      options: ["Dollar", "Mark", "Yen", "Pound"],
      answer: 4,
    },
    {
      text: "What is the height of Mount Everest",
      options: ["9231 m", "8848 m", "8027 m", "8912 m"],
      answer: 2,
    },
    {
      text: "What is the capital of Japan",
      options: ["Beijing", "Osaka", "Kyoto", "Tokyo"],
      answer: 4,
    },
    {
      text: "What is the capital of Egypt",
      options: ["Cairo", "Teheran", "Baghdad", "Dubai"],
      answer: 1,
    },
  ];

  previousQust() {
    this.temp -= 1;
  }

  ngOnInit() {}

  choosedAnswer(index) {
    console.log(index);
    if (index == 0) {
      this.answer = " Your Answer: A";
      this.ans = "A";
    } else if (index == 1) {
      this.answer = " Your Answer: B";
      this.ans = "B";
    } else if (index == 2) {
      this.answer = " Your Answer: C";
      this.ans = "C";
    } else if (index == 3) {
      this.answer = " Your Answer: D";
      this.ans = "D";
    }
  }
  nexQuestion() {
    this.temp += 1;
    if (this.temp == this.questions.length - 1) {
      this.temp = this.questions.length - 1;
    }
    this.answerSheet.push(this.ans);
    // console.log(this.answerSheet);
    this.answer = "Your Answer: Not Answered";
    this.ans = "";

    this.compChanged.emit(this.answerSheet);
  }
}
