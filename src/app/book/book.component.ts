import { BooksNetService } from "./../books-net.service";
import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";

@Component({
    selector: "app-book",
    templateUrl: "./book.component.html",
    styleUrls: ["./book.component.css"],
})
export class BookComponent implements OnInit {
    bestArray = ["Yes", "No"];
    langArray = ["English", "French", "Latin", "Other"];
    bestsell = "";
    language = "";
    bestSelected = "";
    languageSelected = "";
    bestSellerStructure = null;
    lnguageStructure = null;

    cPage: string;
    maxPage = 15;
    type: string = null;
    newArrival: string = null;
    bookData: Object;
    sPage: number = 1;
    ePage: number = 10;
    totalBook: number;
    genreType;
    parameter;
    newJSonArray = [];

    url = "http://localhost:2410/booksapp/books";
    url1 = "";

    constructor(
        private bookService: BooksNetService,
        private route: ActivatedRoute,
        private router: Router
    ) {}

    ngOnInit() {
        this.route.paramMap.subscribe((params) => {
            this.type = params.get("genre");
            console.log(this.type);
        });
        this.route.queryParamMap.subscribe((params) => {
            console.log(params);
            this.language = params.get("language");
            this.bestsell = params.get("bestseller");
            this.newArrival = params.get("newarrival");
            this.genreType = params.get("name");
            this.cPage = params.get("page");
            this.bestsell = params.get("bestseller");
            this.language = params.get("language");
            this.cPage = this.cPage ? this.cPage : "1";

            this.makeStructure();
            this.makeStruct();
        });
    }
    makeUrl() {
        if (this.genreType !== null) {
            this.url = "http://localhost:2410/booksapp/books/" + this.genreType;
        }
        this.url1 = this.url + "?";

        if (this.newArrival !== null) {
            this.url1 = this.url1 + "newarrival=" + this.newArrival + "&";
        }
        if (this.bestsell) {
            this.url1 = this.url1 + "bestseller=" + this.bestsell + "&";
        }
        if (this.language) {
            this.url1 = this.url1 + "language=" + this.language + "&";
        }

        if (this.cPage !== null) {
            this.url1 = this.url1 + "page=" + this.cPage + "&";
        }
    }
    makeStructure() {
        this.makeUrl();

        this.bookService.getData(this.url1).subscribe((resp) => {
            this.bookData = resp.data;

            this.totalBook = resp.pageInfo.totalBook;
            this.maxPage = resp.pageInfo.numberOfPages;
            console.log(resp.data);
            console.log(resp);
        });
    }
    previous(y) {
        if (this.sPage == 0) {
            this.sPage = 1;
        } else {
            this.sPage -= this.bookData.length;
        }

        this.ePage -= this.bookData.length;
        this.cPage = +this.cPage - 1;
        this.pageShow();
    }
    nextPage(x) {
        this.sPage += this.ePage - this.sPage + 1;

        if (this.ePage == this.bookData.length - 7) {
            this.ePage = this.bookData.length - 1;
        } else {
            this.ePage += this.bookData.length;
        }

        this.cPage = +this.cPage + x;
        this.pageShow();
    }
    makeStruct() {
        this.bestSelected = this.bestsell ? this.bestsell : "";
        this.languageSelected = this.language ? this.language : "";

        this.bestSellerStructure = this.bestArray.map((p) => ({
            bestseller: p,
            selected: false,
        }));
        let temp1 = this.bestSelected.split(",");
        for (let i = 0; i < temp1.length; i++) {
            let item = this.bestSellerStructure.find(
                (p1) => p1.bestseller == temp1[i]
            );
            if (item) item.selected = true;
        }
        this.lnguageStructure = this.langArray.map((p) => ({
            language: p,
            selected: false,
        }));
        let temp2 = this.languageSelected.split(",");
        for (let i = 0; i < temp2.length; i++) {
            let item = this.lnguageStructure.find(
                (p1) => p1.language == temp2[i]
            );
            if (item) item.selected = true;
        }
        console.log(this.bestSellerStructure);
        console.log(this.lnguageStructure);
    }
    pageShow() {
        if (this.cPage) {
            this.newJSonArray.push({ page: this.cPage });
        }
        if (this.newArrival) {
            this.newJSonArray.push({ newarrival: this.newArrival });
        }

        if (this.bestsell != "") {
            this.newJSonArray.push({ bestseller: this.bestsell });
        }
        if (this.language != "") {
            this.newJSonArray.push({ language: this.language });
        }

        this.parameter = this.newJSonArray.reduce((arr, item) =>
            Object.assign(arr, item, {})
        );
        console.log(this.parameter);

        this.router.navigate([], {
            queryParams: this.parameter,
        });
    }

    optChange() {
        let bl = this.bestSellerStructure.filter((p1) => p1.selected);
        let lS = this.lnguageStructure.filter((p1) => p1.selected);
        let t1 = bl.map((p1) => p1.bestseller);
        let t2 = lS.map((p1) => p1.language);
        this.bestSelected = t1.join(",");
        this.languageSelected = t2.join(",");
        this.cPage = "1";

        let qparams = {};
        if (this.bestSelected) qparams["bestseller"] = this.bestSelected;
        if (this.languageSelected) qparams["language"] = this.languageSelected;

        this.router.navigate([], { queryParams: qparams });
        this.makeStructure();
        this.sPage = 1;
        this.ePage = 10;
    }
}
