import { Component, OnInit } from "@angular/core";
import { NetService } from "../net.service";
import { ParamService } from "../param.service";
import { Router, ActivatedRoute, ParamMap } from "@angular/router";
@Component({
    selector: "app-books",
    templateUrl: "./books.component.html",
    styleUrls: ["./books.component.css"],
})
export class BooksComponent implements OnInit {
    url = "http://localhost:2410/booksapp/books";
    url2;
    url3;
    strData: any[] = [];
    constructor(
        private netService: NetService,
        private paramService: ParamService,
        private route: ActivatedRoute,
        private router: Router
    ) {}

    ngOnInit() {
        // this.route.paramMap.subscribe((params) => {
        //     console.log(params);
        // });
        this.route.paramMap.subscribe((params) => {
            let type = params.get("selectbook");
            console.log(type);
            if (this.paramService.parameter == "All") {
                this.url = this.url;
                this.makeStruct();
            }
            if (this.paramService.parameter == "Cricket") {
                this.url = "http://localhost:2410/sporticons/stars";
                this.url = "http://localhost:2410/sporticons/stars/Cricket";
                this.makeStruct();
            }
            if (this.paramService.parameter == "Football") {
                this.url = "http://localhost:2410/sporticons/stars";
                this.url = "http://localhost:2410/sporticons/stars/Football";
                this.makeStruct();
            }
        });
    }
    makeStruct() {
        this.netService.getData(this.url).subscribe((resp) => {
            this.strData.push(resp.data);
            console.log(resp.data);
            console.log(this.strData);
        });
    }
}
