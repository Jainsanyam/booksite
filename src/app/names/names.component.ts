import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-names',
  templateUrl: './names.component.html',
  styleUrls: ['./names.component.css'],
})
export class NamesComponent implements OnInit {
  names = ['Jack', 'Steve', 'William', 'Kathy', 'Edward'];
  namesIds = [
    { name: 'Jack', id: 'AB455' },
    { name: 'Steve', id: 'GM072' },
    { name: 'Wlliam', id: 'CX499' },
    { name: 'Karthy', id: 'MM746' },
    { name: 'Edward', id: 'KT108' },
  ];
  namesRadioStructure = null;
  nameRadioSelected = '';
  constructor() {}

  ngOnInit(): void {
    this.UpdateValues();
  }

  UpdateValues() {
    this.namesRadioStructure = {
      names: this.names,
      selected: '',
    };
  }

  optChange() {
    this.nameRadioSelected = this.namesRadioStructure.selected;
  }
}
