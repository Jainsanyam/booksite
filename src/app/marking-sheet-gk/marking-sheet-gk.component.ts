import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-marking-sheet-gk",
  templateUrl: "./marking-sheet-gk.component.html",
  styleUrls: ["./marking-sheet-gk.component.css"],
})
export class MarkingSheetGkComponent implements OnInit {
  @Input() ansSheet: any;
  constructor() {}

  ngOnInit() {
    console.log(this.ansSheet);
  }
}
