import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarkingSheetGkComponent } from './marking-sheet-gk.component';

describe('MarkingSheetGkComponent', () => {
  let component: MarkingSheetGkComponent;
  let fixture: ComponentFixture<MarkingSheetGkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarkingSheetGkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarkingSheetGkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
