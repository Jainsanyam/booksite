import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css'],
})
export class TeamComponent implements OnInit {
  Details: any = [
    {
      name: 'Realme 2',
      brand: 'Realme',
      RAM: '3GB',
      ROM: '32GB',
      price: 7999,
    },

    {
      name: 'Realme 3',
      brand: 'Realme',
      RAM: '4GB',
      ROM: '64GB',
      price: 9999,
    },

    {
      name: 'Realme Pro',
      brand: 'Realme',
      RAM: '4GB',
      ROM: '64GB',
      price: 10299,
    },
  ];
  playersArray = ['3GB', '4GB', '6GB'];
  sportsArrya = ['Below 10,000', '10,000 or More'];
  team = 'ABC';
  page = '';
  players = '';
  sport = '';
  playersStructure = null;
  sportsStructure = null;
  playersSelected = '';
  currPage = 1;
  maxPage = 10;

  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.route.paramMap.subscribe((params) => {
      this.team = params.get('team');
    });
    this.route.queryParamMap.subscribe((params) => {
      console.log(params);
      this.page = params.get('page');
      this.players = params.get('players');
      this.sport = params.get('sport');
      this.makeStruct();
    });
  }
  makeStruct() {
    this.currPage = this.page ? +this.page : 1;
    this.playersSelected = this.players ? this.players : '';
    this.playersStructure = this.playersArray.map((ply1) => ({
      name: ply1,
      selected: false,
    }));
    let temp1 = this.playersSelected.split(',');
    for (let i = 0; i < temp1.length; i++) {
      let item = this.playersStructure.find((p1) => p1.name == temp1[i]);
      if (item) item.selected = true;
    }
    this.sportsStructure = {
      sports: this.sportsArrya,
      selected: this.sport ? this.sport : '',
    };
    console.log(this.playersStructure);
    console.log(this.sportsStructure);
  }

  optChange() {
    let temp1 = this.playersStructure.filter((p1) => p1.selected);
    let temp2 = temp1.map((p1) => p1.name);
    this.playersSelected = temp2.join(',');
    let path = '/match/' + this.team;

    let qparams = {};
    if (this.playersSelected) qparams['players'] = this.playersSelected;

    if (this.sportsStructure.selected)
      qparams['sport'] = this.sportsStructure.selected;
    this.router.navigate([path], { queryParams: qparams });
  }
  goToPage(x) {
    this.currPage = this.currPage + x;
    let path = '/match/' + this.team;
    this.router.navigate([path], {
      queryParams: { page: this.currPage },
      queryParamsHandling: 'merge',
    });
  }
}
